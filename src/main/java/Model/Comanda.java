package Model;

import java.sql.Date;
import java.util.ArrayList;

public class Comanda {
    private int idComanda;
    private Client client;
    private ArrayList<Subcomanda> subcomenzi=new ArrayList<Subcomanda>();
    private float total;
    private Date date;

    public  Comanda(){

    }
    public Comanda(Client c,int idComanda){
        this.client=c;
        this.idComanda=idComanda;

    }

    public void setIdComanda(int idComanda) {
        this.idComanda = idComanda;
    }

    public void setSubcomenzi(ArrayList<Subcomanda> subcomenzi) {
        this.subcomenzi = subcomenzi;
    }

    public void setTotal(float total) {
        this.total = total;
    }

    public void setClient(Client client) {
        this.client = client;
    }


    public Client getClient() {
        return client;
    }

    public void addSubcomanda(Subcomanda subcomanda){
        subcomenzi.add(subcomanda);
    }

    public ArrayList<Subcomanda> getSubcomenzi() {
        return subcomenzi;
    }

    public float getTotal() {
        return total;
    }

    public Date getDate() {
        return date;
    }

    public int getIdComanda() {
        return idComanda;
    }
}
