package Model;

public class Subcomanda {
    private int idSubcomanda;
    private Produs produs;
    private int idComanda;
    private int cantitate;

    public Subcomanda(){

    }

    public Subcomanda(int idComanda,int idSubcomanda,Produs produs,int cantitate){
        this.idComanda=idComanda;
        this.produs=produs;
        this.idSubcomanda=idSubcomanda;
        this.cantitate=cantitate;
    }

    public void setIdSubcomanda(int idSubcomanda) {
        this.idSubcomanda = idSubcomanda;
    }

    public void setCantitate(int cantitate) {
        this.cantitate = cantitate;
    }

    public void setProdus(Produs produs) {
        this.produs = produs;
    }

    public int getIdComanda() {
        return idComanda;
    }

    public Produs getProdus() {
        return produs;
    }

    public int getCantitate() {
        return cantitate;
    }

    public int getIdSubcomanda() {
        return idSubcomanda;
    }
}
