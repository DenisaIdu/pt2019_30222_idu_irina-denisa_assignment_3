package Model;

public class Produs {
    private int idProdus;
    private String nume;
    private int stoc;
    private float pret;

    public Produs(String nume,int stoc,float pret){
        this.nume=nume;
        this.stoc=stoc;
        this.pret=pret;
    }

    public void setIdProdus(int idProdus) {
        this.idProdus = idProdus;
    }

    public String getNume() {
        return nume;
    }

    public float getPret() {
        return pret;
    }

    public int getIdProdus() {
        return idProdus;
    }

    public int getStoc() {
        return stoc;
    }
}
