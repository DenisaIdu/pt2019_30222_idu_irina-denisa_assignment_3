package Model;

public class Admin {
    private String nume;
    private String parola;

    public Admin(String nume,String parola){
        this.nume=nume;
        this.parola=parola;
    }

    public String getNume() {
        return nume;
    }

    public String getParola() {
        return parola;
    }
}
