package Model;

public class Client {
    private int idClient;
    private String nume;
    private String prenume;
    private String parola;
    private String email;
    private String adress;

    public Client(){

    }

    public  Client(String nume,String prenume,String parola,String email,String adress){
        this.idClient=-1;
        this.nume=nume;
        this.prenume=prenume;
        this.parola=parola;
        this.adress=adress;
        this.email=email;
    }

    public void setNume(String nume) {
        this.nume = nume;
    }

    public int getIdClient() {
        return idClient;
    }

    public String getNume() {
        return nume;
    }

    public void setIdClient(int idClient) {
        this.idClient = idClient;
    }

    public String getPrenume() {
        return prenume;
    }

    public String getParola() {
        return parola;
    }

    public String getAdress() {
        return adress;
    }

    public String getEmail() {
        return email;
    }
}
