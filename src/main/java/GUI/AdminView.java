package GUI;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class AdminView {

    JFrame frame;
    JButton btn1,btn2,btn3,btn4,btn5,btn6;

    public AdminView(){
        frame = new JFrame("Client Form");
        frame.setVisible(true);
        frame.setSize(500,300);
        frame.setLayout(null);

        btn1=new JButton("View Clients");
        btn2=new JButton("Delete Clients");
        btn3=new JButton("View Products");
        btn4=new JButton("Delete Product");
        btn5=new JButton("Add product");
        btn6=new JButton("Edit product");

        btn1.setBounds(50,50,150,25);
        btn2.setBounds(230,50,150,25);

        btn3.setBounds(50,150,150,25);
        btn4.setBounds(230,150,150,25);
        btn5.setBounds(50,200,150,25);
        btn6.setBounds(230,200,150,25);

        frame.add(btn1);
        frame.add(btn2);
        frame.add(btn3);
        frame.add(btn4);
        frame.add(btn5);
        frame.add(btn6);

        btn1.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                ShowClientsView clientView=new ShowClientsView();
            }
        });

        btn2.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                DeleteClientView deleteClientView=new DeleteClientView();
            }
        });

        btn4.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                DeleteProductView dpv=new DeleteProductView();
            }
        });

        btn5.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                AddProductView addProductView=new AddProductView();
            }
        });

        btn3.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                ShowProduseView showProduseView =new ShowProduseView();
            }
        });

        btn6.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                EditProductView editProductView=new EditProductView();
            }
        });




    }
}
