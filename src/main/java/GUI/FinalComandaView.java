package GUI;

import Model.Comanda;
import Model.Produs;
import Model.Subcomanda;
import dataAccessLayer.ComandaDAO;
import dataAccessLayer.ProdusDAO;
import dataAccessLayer.SubcomandaDAO;
import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;

public class FinalComandaView {
    JFrame frame;
    public FinalComandaView(Comanda comanda){
        frame = new JFrame("Show all products");
        frame.setVisible(true);
        frame.setSize(500,500);


        String data[][]=new String[comanda.getSubcomenzi().size()+1][100];
        String column[]={"Produs","Cantitate","Pret","Total"};

        for (int i=0;i<comanda.getSubcomenzi().size();i++){
            data[i][0]=comanda.getSubcomenzi().get(i).getProdus().getNume();
            data[i][1]= String.valueOf(comanda.getSubcomenzi().get(i).getCantitate());
            data[i][2]= String.valueOf(comanda.getSubcomenzi().get(i).getProdus().getPret());
            data[i][3]= String.valueOf(comanda.getSubcomenzi().get(i).getCantitate()*comanda.getSubcomenzi().get(i).getProdus().getPret());
        }
        data[comanda.getSubcomenzi().size()][3]= String.valueOf(comanda.getTotal());



        JTable jTable=new JTable(data,column);
        jTable.setBounds(30,40,200,300);
        JScrollPane scrollPane=new JScrollPane(jTable);
        frame.add(scrollPane);

    }
}
