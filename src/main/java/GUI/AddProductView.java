package GUI;

import Model.Produs;
import businessLayer.ProdusBL;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class AddProductView {
    JFrame frame;
    JLabel l1,l2,l3;
    JTextField tf1,tf2,tf3;
    JButton btn1,btn2;
    public AddProductView(){
        frame = new JFrame("AddProducts");
        frame.setVisible(true);
        frame.setSize(500,500);
        frame.setLayout(null);

        l1=new JLabel("Nume: ");
        l2=new JLabel("Stoc: ");
        l3=new JLabel("Pret: ");

        tf1=new JTextField();
        tf2=new JTextField();
        tf3=new JTextField();

        btn1=new JButton("Add");
        btn2=new JButton("Clear");

        l1.setBounds(50,50,150,30);
        tf1.setBounds(200,50,150,50);
        l2.setBounds(50,130,150,30);
        tf2.setBounds(200,130,150,50);
        l3.setBounds(50,200,150,50);
        tf3.setBounds(200,200,150,50);
        btn1.setBounds(100,300,100,50);
        btn2.setBounds(250,300,100,50);


        frame.add(l1);
        frame.add(l2);
        frame.add(l3);
        frame.add(tf1);
        frame.add(tf2);
        frame.add(tf3);
        frame.add(btn1);
        frame.add(btn2);

        btn1.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                ProdusBL produsBL=new ProdusBL();
                produsBL.addProdus(AddProductView.this);
            }
        });

        btn2.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                tf1.setText("");
                tf2.setText("");
                tf3.setText("");
            }
        });
    }

    public JTextField getTf1() {
        return tf1;
    }

    public JTextField getTf2() {
        return tf2;
    }

    public JTextField getTf3() {
        return tf3;
    }
}
