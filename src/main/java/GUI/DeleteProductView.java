package GUI;

import Model.Client;
import Model.Produs;
import businessLayer.ClientBL;
import businessLayer.ProdusBL;
import dataAccessLayer.ClientDAO;
import dataAccessLayer.ProdusDAO;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

public class DeleteProductView {
    JFrame frame;
    JComboBox comboBox;
    JLabel l1;
    JButton btn1;
    public DeleteProductView(){
        frame = new JFrame("AddProducts");
        frame.setVisible(true);
        frame.setSize(500,350);
        frame.setLayout(null);


        ArrayList<Produs> produse;
        ProdusDAO produsDAO=new ProdusDAO();
        produse=produsDAO.showAllProduct();
        String nume[]=new String[produse.size()];

        for (int i=0;i<produse.size();i++){
            nume[i]=produse.get(i).getNume();
        }

        comboBox=new JComboBox(nume);
        l1=new JLabel("Produsul pe care vreti sa il stergeti: ");
        btn1=new JButton("Sterge");

        l1.setBounds(50,50,200,50);
        comboBox.setBounds(100,100,150,50);
        btn1.setBounds(200,200,150,30);

        frame.add(comboBox);
        frame.add(btn1);
        frame.add(l1);


        btn1.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                ProdusBL produsBL=new ProdusBL();
                produsBL.delete(comboBox.getSelectedItem().toString());
                frame.setVisible(false);
            }
        });

    }

    public JComboBox getComboBox() {
        return comboBox;
    }
}
