package GUI;

import Model.Client;
import Model.Comanda;
import Model.Produs;
import Model.Subcomanda;
import businessLayer.ClientBL;
import businessLayer.ComandaBL;
import dataAccessLayer.ProdusDAO;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

public class ComandaView {
    JFrame frame;
    JButton btn1,btn2;
    JTextField tf1;
    JLabel l1,l2;
    JComboBox comboBox;

    public ComandaView(final Client c) {
        frame = new JFrame("Show all products");
        frame.setVisible(true);
        frame.setSize(500,500);
        frame.setLayout(null);

        l1=new JLabel("Cantitate");
        l2=new JLabel("Selecteaza produsul");


        ArrayList<Produs> products=new ArrayList<Produs>();
        ProdusDAO produsDAO=new ProdusDAO();
        products=produsDAO.showAllProduct();

        String produse[]=new String[products.size()];
        for (int i=0;i<products.size();i++){
            produse[i]=products.get(i).getNume();
        }

        comboBox=new JComboBox(produse);
        tf1=new JTextField("1");
        btn1=new JButton("Add");
        btn2=new JButton("Finalizare");

        l2.setBounds(50,50,150,30);
        comboBox.setBounds(100,100,150,30);
        l1.setBounds(50,150,100,30);
        tf1.setBounds(275,150,100,30);
        btn1.setBounds(300,200,100,30);
        btn2.setBounds(300,275,100,30);

        frame.add(l1);
        frame.add(l2);
        frame.add(btn1);
        frame.add(btn2);
        frame.add(comboBox);
        frame.add(tf1);

        final Comanda comanda=new Comanda();
        comanda.setClient(c);

        btn1.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                ProdusDAO produsDAO1=new ProdusDAO();
                Produs produs=produsDAO1.findByName(comboBox.getSelectedItem().toString());
                int cantitate= Integer.parseInt(tf1.getText());

                if (cantitate<produs.getStoc()){
                    Subcomanda subcomanda=new Subcomanda();
                    subcomanda.setProdus(produs);
                    subcomanda.setCantitate(cantitate);
                    comanda.addSubcomanda(subcomanda);
                    comanda.setTotal(comanda.getTotal()+produs.getPret()*cantitate);
                    produsDAO1.updateStoc(produs.getNume(),produs.getStoc()-cantitate);


                }
                else{
                    JOptionPane.showMessageDialog(null, "Stoc insuficient!");
                }


            }
        });

        btn2.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                ComandaBL comandaBL=new ComandaBL();
                comandaBL.addComada(comanda);
                FinalComandaView finalComandaView=new FinalComandaView(comanda);

            }
        });






    }
}
