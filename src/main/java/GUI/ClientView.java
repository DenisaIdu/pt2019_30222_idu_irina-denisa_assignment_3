package GUI;

import Model.Client;
import dataAccessLayer.ClientDAO;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

public class ClientView {
    JFrame frame;
    JButton btn1,btn2;

    public ClientView(final Client c){
        frame = new JFrame("Client Form");
        frame.setVisible(true);
        frame.setSize(300,300);
        frame.setLayout(null);

        btn1=new JButton("Edit ");
        btn2=new JButton("Produse");

        btn1.setBounds(75,75,100,50);
        btn2.setBounds(75,150,100,50);

        frame.add(btn1);
        frame.add(btn2);

        btn1.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                EditClientView editClientView=new EditClientView(c.getNume());
            }
        });

        btn2.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                ComandaView comandaView=new ComandaView(c);
            }
        });

    }
}
