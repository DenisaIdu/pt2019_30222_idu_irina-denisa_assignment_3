package GUI;

import Model.Produs;
import businessLayer.ProdusBL;
import dataAccessLayer.ProdusDAO;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

public class EditProductView {

    JFrame frame;
    JComboBox comboBox1,comboBox2;
    JLabel l1,l2,l3;
    JButton btn1;
    JTextField tf;
    public EditProductView() {
        frame = new JFrame("AddProducts");
        frame.setVisible(true);
        frame.setSize(500, 500);
        frame.setLayout(null);

        ArrayList<Produs> produse;
        ProdusDAO produsDAO=new ProdusDAO();
        produse=produsDAO.showAllProduct();
        String nume[]=new String[produse.size()];
        for (int i=0;i<produse.size();i++){
            nume[i]=produse.get(i).getNume();
        }

        comboBox1=new JComboBox(nume);
        String opt[]={"Stoc","Pret"};
        comboBox2=new JComboBox(opt);

        l1=new JLabel("Produdul care il modificati");
        l2=new JLabel("Campul care se modifica");
        l3=new JLabel("Valoarea modificata");
        tf=new JTextField();
        btn1=new JButton("Save");

        l1.setBounds(50,50,200,30);
        comboBox1.setBounds(100,100,100,30);
        l2.setBounds(50,150,150,30);
        comboBox2.setBounds(100,200,150,30);
        l3.setBounds(50,250,150,30);
        tf.setBounds(100,300,100,30);
        btn1.setBounds(300,350,100,30);

        frame.add(l1);
        frame.add(l2);
        frame.add(l3);
        frame.add(comboBox1);
        frame.add(comboBox2);
        frame.add(tf);
        frame.add(btn1);

        btn1.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                ProdusBL produsBL=new ProdusBL();
                produsBL.edit(comboBox1.getSelectedItem().toString(),comboBox2.getSelectedItem().toString(),tf.getText());
                frame.setVisible(false);
            }
        });


    }
}
