package GUI;

import Model.Produs;
import businessLayer.ProdusBL;
import dataAccessLayer.ClientDAO;
import dataAccessLayer.ProdusDAO;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

public class EditClientView {
    JFrame frame;
    JComboBox comboBox2;
    JLabel l2,l3;
    JButton btn1;
    JTextField tf;
    public EditClientView(final String name) {
        frame = new JFrame("Edit");
        frame.setVisible(true);
        frame.setSize(500, 450);
        frame.setLayout(null);

        ArrayList<Produs> produse;
        ProdusDAO produsDAO=new ProdusDAO();
        produse=produsDAO.showAllProduct();
        String nume[]=new String[produse.size()];
        for (int i=0;i<produse.size();i++){
            nume[i]=produse.get(i).getNume();
        }

        String opt[]={"Email","Parola","Adresa"};
        comboBox2=new JComboBox(opt);

        l2=new JLabel("Campul care se modifica");
        l3=new JLabel("Valoarea modificata");
        tf=new JTextField();
        btn1=new JButton("Save");


        l2.setBounds(50,50,150,30);
        comboBox2.setBounds(100,100,150,30);
        l3.setBounds(50,150,150,30);
        tf.setBounds(100,200,100,30);
        btn1.setBounds(300,300,100,30);

        frame.add(l2);
        frame.add(l3);
        frame.add(comboBox2);
        frame.add(tf);
        frame.add(btn1);

        btn1.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                String camp=comboBox2.getSelectedItem().toString();
                String modifica=tf.getText();
                if (camp.equals("Adresa")){
                    ClientDAO clientDAO=new ClientDAO();
                    clientDAO.updateAdresa(name,modifica);
                }else{
                    if (camp.equals("Email")){
                        ClientDAO clientDAO=new ClientDAO();
                        clientDAO.updateEmail(name,modifica);
                    }else{
                        if (camp.equals("Parola")) {
                            ClientDAO clientDAO = new ClientDAO();
                            clientDAO.updateParola(name, modifica);
                        }
                    }

                }
                frame.setVisible(false);


            }
        });


    }
}
