package GUI;

import businessLayer.Register;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class RegisterView {

    JLabel l1, l3, l4, l6, l7, l8;
    JTextField tf2, tf5, tf6, tf7;
    JButton btn1, btn2;
    JPasswordField p1;
    JFrame frame;

    public RegisterView(){
        frame=new JFrame();
        frame.setVisible(true);
        frame.setSize(700, 500);
        frame.setLayout(null);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setTitle("Registration");
        l1 = new JLabel("Registration");
        l1.setForeground(Color.blue);
        l1.setFont(new Font("Serif", Font.BOLD, 20));


        l3 = new JLabel("Nume:");
        l4 = new JLabel("Create Passowrd:");
        l6 = new JLabel("Prenume:");
        l7=new JLabel("Email:");
        l8=new JLabel("Adress: ");



        tf2 = new JTextField(" ");
        p1 = new JPasswordField();
        tf5 = new JTextField(" ");
        tf6=new JTextField(" ");
        tf7=new JTextField(" ");


        btn1 = new JButton("Submit");
        btn2 = new JButton("Clear");

        l1.setBounds(100, 30, 400, 30);
        l3.setBounds(80, 110, 200, 30);
        l4.setBounds(80, 150, 200, 30);
        l6.setBounds(80, 230, 200, 30);
        l7.setBounds(80,270,200,30);
        l8.setBounds(80,310,200,30);



        tf2.setBounds(300, 110, 200, 30);
        p1.setBounds(300, 150, 200, 30);
        tf5.setBounds(300, 230, 200, 30);
        tf6.setBounds(300,270,200,30);
        tf7.setBounds(300,310,200,30);


        btn1.setBounds(50, 400, 100, 30);
        btn2.setBounds(170, 400, 100, 30);
        frame.add(l1);

        frame.add(l3);
        frame.add(tf2);
        frame.add(l4);
        frame.add(p1);
        frame.add(l6);
        frame.add(l7);
        frame.add(l8);
        frame.add(tf5);
        frame.add(tf6);
        frame.add(tf7);
        frame.add(btn1);
        frame.add(btn2);

        btn1.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                Register register=new Register(RegisterView.this);
                frame.setVisible(false);
            }
        });

        btn2.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                tf2.setText("");
                tf5.setText("");
                tf6.setText("");
                tf7.setText("");
            }
        });
    }



    public JTextField getTf5() {
        return tf5;
    }

    public JTextField getTf2() {
        return tf2;
    }

    public JTextField getTf6() {
        return tf6;
    }

    public JTextField getTf7() {
        return tf7;
    }

    public JPasswordField getP1() {
        return p1;
    }
}
