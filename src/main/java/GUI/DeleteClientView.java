package GUI;

import Model.Client;
import businessLayer.ClientBL;
import businessLayer.ProdusBL;
import dataAccessLayer.ClientDAO;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

public class DeleteClientView {

    JFrame frame;
    JComboBox comboBox;
    JLabel l1;
    JButton btn1;
    public DeleteClientView(){
        frame = new JFrame("AddProducts");
        frame.setVisible(true);
        frame.setSize(500,350);
        frame.setLayout(null);


        ArrayList<Client> clients=new ArrayList<Client>();
        ClientDAO clientDAO=new ClientDAO();
        clients=clientDAO.showAllClints();
        String nume[]=new String[clients.size()];

        for (int i=0;i<clients.size();i++){
            nume[i]=clients.get(i).getNume();
        }

        comboBox=new JComboBox(nume);
        l1=new JLabel("Clientul pe care vreti sa il stergeti: ");
        btn1=new JButton("Sterge");

        l1.setBounds(50,50,200,50);
        comboBox.setBounds(100,100,150,50);
        btn1.setBounds(200,200,150,30);

        frame.add(comboBox);
        frame.add(btn1);
        frame.add(l1);


        btn1.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                ClientBL clientBL=new ClientBL();
                clientBL.delete(comboBox.getSelectedItem().toString());
                frame.setVisible(false);
            }
        });

    }

    public JComboBox getComboBox() {
        return comboBox;
    }
}
