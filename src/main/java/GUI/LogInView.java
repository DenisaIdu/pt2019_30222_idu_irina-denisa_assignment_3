package GUI;

import Model.Admin;
import businessLayer.ClientBL;
import dataAccessLayer.AdminDAO;
import sun.rmi.runtime.Log;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class LogInView {
    JLabel l1, l2, l3;
    JTextField tf1;
    JButton btn1;
    JButton btn2;
    JButton btn3;
    JPasswordField p1;
    JFrame frame;
    public  LogInView() {
        frame = new JFrame("Login Form");
        l1 = new JLabel("Login");
        l1.setForeground(Color.blue);
        l1.setFont(new Font("Serif", Font.BOLD, 20));
        frame.setBackground(Color.darkGray);


        l2 = new JLabel("Username");
        l3 = new JLabel("Password");
        tf1 = new JTextField();
        p1 = new JPasswordField();
        btn1 = new JButton("Login");
        btn2 = new JButton("Login as Admin");
        btn3 = new JButton("Register now");


        l1.setBounds(200, 50, 400, 30);
        l2.setBounds(130, 150, 200, 30);
        l3.setBounds(130, 200, 200, 30);
        tf1.setBounds(230, 150, 200, 30);
        p1.setBounds(230, 200, 200, 30);
        btn1.setBounds(50, 270, 100, 30);
        btn2.setBounds(180, 270, 150, 30);
        btn3.setBounds(360, 270, 150, 30);

        frame.add(l1);
        frame.add(l2);
        frame.add(tf1);
        frame.add(l3);
        frame.add(p1);
        frame.add(btn1);
        frame.add(btn2);
        frame.add(btn3);

        btn1.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                ClientBL clientBL=new ClientBL();
                clientBL.logInClient(LogInView.this);


            }
        });

        btn2.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                AdminDAO adminDAO=new AdminDAO();
                Admin admin=adminDAO.findByNume(tf1.getText());
                if (admin.getParola().equals(p1.getText())){
                    AdminView adminView=new AdminView();
                }else{
                    JOptionPane.showMessageDialog(null, "invalid password");
                }

            }
        });

        btn3.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                RegisterView registerView=new RegisterView();
            }
        });
        frame.setSize(600, 400);
        frame.setLayout(null);
        frame.setVisible(true);
    }

    public JTextField getTf1() {
        return tf1;
    }

    public JPasswordField getP1() {
        return p1;
    }

    public void wrongPass(){
        JOptionPane.showMessageDialog(null, "invalid password");
    }
}
