package GUI;

import Model.Client;
import Model.Produs;
import dataAccessLayer.ClientDAO;
import dataAccessLayer.ProdusDAO;

import javax.swing.*;
import java.util.ArrayList;

public class ShowProduseView {
    JFrame frame;
    public ShowProduseView(){
        frame = new JFrame("Show all products");
        frame.setVisible(true);
        frame.setSize(500,500);



        ArrayList<Produs> products=new ArrayList<Produs>();
        ProdusDAO produsDAO=new ProdusDAO();
        products=produsDAO.showAllProduct();

        String data[][]=new String[products.size()][100];
        String column[]={"ID","Nume","Stoc","Pret"};

        for (int i=0;i<products.size();i++){
            data[i][0]= String.valueOf(products.get(i).getIdProdus());
            data[i][1]=products.get(i).getNume();
            data[i][2]= String.valueOf(products.get(i).getStoc());
            data[i][3]= String.valueOf(products.get(i).getPret());

        }

        JTable jTable=new JTable(data,column);
        jTable.setBounds(30,40,200,300);
        JScrollPane scrollPane=new JScrollPane(jTable);
        frame.add(scrollPane);

    }
}
