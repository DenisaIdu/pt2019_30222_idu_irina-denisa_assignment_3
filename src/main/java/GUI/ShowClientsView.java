package GUI;

import Model.Client;
import dataAccessLayer.ClientDAO;

import javax.swing.*;
import java.util.ArrayList;

public class ShowClientsView {
    JFrame frame;
    public ShowClientsView(){
        frame = new JFrame("Client Form");
        frame.setVisible(true);
        frame.setSize(500,500);

        ArrayList<Client> clients=new ArrayList<Client>();
        ClientDAO clientDAO=new ClientDAO();
        clients= clientDAO.showAllClints();

        String data[][]=new String[clients.size()][100];
        String column[]={"ID","Nume","Prenume","Parola","Email","Adress"};

        for (int i=0;i<clients.size();i++){
            data[i][0]= String.valueOf(clients.get(i).getIdClient());
            data[i][1]=clients.get(i).getNume();
            data[i][2]=clients.get(i).getPrenume();
            data[i][3]=clients.get(i).getParola();
            data[i][4]=clients.get(i).getAdress();
            data[i][5]=clients.get(i).getEmail();

        }

        JTable jTable=new JTable(data,column);
        jTable.setBounds(30,40,200,300);
        JScrollPane scrollPane=new JScrollPane(jTable);
        frame.add(scrollPane);



    }
}
