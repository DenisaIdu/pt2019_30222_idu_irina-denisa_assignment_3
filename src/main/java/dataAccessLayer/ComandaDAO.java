package dataAccessLayer;

import Model.Client;
import Model.Comanda;
import Model.Subcomanda;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

public class ComandaDAO {

    public void addComanda(Comanda comanda){
        PreparedStatement ps=null;
        ResultSet rs=null;
        Connection conn=null;

        try{
            conn=ConnectionFactory.getConnection();
            String query="INSERT INTO comanda(idClient, total) VALUES(?,?)";
            ps=conn.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
            ps.setInt(1,comanda.getClient().getIdClient());
            ps.setFloat(2,comanda.getTotal());
            ps.execute();
            rs=ps.getGeneratedKeys();
            if(rs.next()){
                comanda.setIdComanda(rs.getInt(1));
                SubcomandaDAO subcomandaDAO=new SubcomandaDAO();
                for (int i=0;i<comanda.getSubcomenzi().size();i++){
                    subcomandaDAO.addSubcomanda(comanda.getSubcomenzi().get(i),comanda);
                }

            }

        }catch (Exception e){
            e.printStackTrace();
        }
    }








}
