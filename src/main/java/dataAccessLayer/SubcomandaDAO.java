package dataAccessLayer;

import Model.Comanda;
import Model.Produs;
import Model.Subcomanda;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

public class SubcomandaDAO {
    public void addSubcomanda(Subcomanda subcomanda, Comanda comanda){
        PreparedStatement ps=null;
        ResultSet rs=null;
        Connection conn=null;
        try{
            conn=ConnectionFactory.getConnection();
            String query="INSERT INTO Subcomanda(idProdus, idComanda, cantitate) VALUES(?,?,?)";
            ps=conn.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
            ps.setInt(1,subcomanda.getProdus().getIdProdus());
            ps.setInt(2,comanda.getIdComanda());
            ps.setInt(3,subcomanda.getCantitate());
            ps.execute();
            rs=ps.getGeneratedKeys();
            if (rs.next()){
                subcomanda.setIdSubcomanda(rs.getInt(1));

            }

        }catch (Exception e){
            e.printStackTrace();

        }
    }

}
