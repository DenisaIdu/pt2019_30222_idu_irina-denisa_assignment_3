package dataAccessLayer;

import Model.Admin;
import Model.Client;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class AdminDAO {
    public Admin findByNume(String nume){

       Admin admin=null;
        Connection conn=null;
        PreparedStatement ps=null;
        ResultSet rs=null;
        try {
            conn=ConnectionFactory.getConnection();
            String query="SELECT idAdmin, parola FROM Adminn where idAdmin=?";
            ps=conn.prepareStatement(query);
            ps.setString(1,nume);
            ps.execute();
            rs=ps.getResultSet();

            if (rs!=null && rs.next()){
                String parola=rs.getString("parola");
                admin=new Admin(nume,parola);

                return admin;
            }

        }catch (Exception e){
            e.printStackTrace();
        }finally {
            ConnectionFactory.close(rs);
            ConnectionFactory.close(ps);
            ConnectionFactory.close(conn);
        }



        return admin;
    }
}
