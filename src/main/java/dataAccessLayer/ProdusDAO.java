package dataAccessLayer;

import Model.Client;
import Model.Produs;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

public class ProdusDAO {

    public Produs add(Produs produs){
        Connection conn=null;
        PreparedStatement ps=null;
        ResultSet rs=null;
        try{
            conn=ConnectionFactory.getConnection();
            String add = "INSERT INTO produs(nume, stoc, pret) VALUES(?, ?, ?)";
            ps=conn.prepareStatement(add, Statement.RETURN_GENERATED_KEYS);
            ps.setString(1,produs.getNume());
            ps.setString(2, String.valueOf(produs.getStoc()));
            ps.setString(3, String.valueOf(produs.getPret()));

            ps.execute();
            rs=ps.getGeneratedKeys();
            if (rs.next()){
                produs.setIdProdus(rs.getInt(1));
            }

        }catch (Exception e){
            e.printStackTrace();
        }finally {
            ConnectionFactory.close(rs);
            ConnectionFactory.close(ps);
            ConnectionFactory.close(conn);
        }
        return produs;
    }

    public ArrayList<Produs> showAllProduct(){
        Connection conn=null;
        PreparedStatement ps=null;
        ResultSet rs=null;

        ArrayList<Produs> produse=new ArrayList<Produs>();
        try{
            conn=ConnectionFactory.getConnection();
            String query="SELECT * FROM Produs";
            ps=conn.prepareStatement(query);
            ps.execute();
            rs=ps.getResultSet();

            while (rs.next()){
                int id=rs.getInt("idProdus");
                String name=rs.getString("nume");
                int stoc= Integer.parseInt(rs.getString("stoc"));
                float pret= Float.parseFloat(rs.getString("pret"));


                Produs produs=new Produs(name,stoc,pret);
                produs.setIdProdus(id);
                produse.add(produs);

            }
        }catch (Exception e){
            e.printStackTrace();
        }finally {

            ConnectionFactory.close(rs);
            ConnectionFactory.close(ps);
            ConnectionFactory.close(conn);
        }

        return produse;
    }

    public Produs findByName(String nume){
        Produs produs=null;
        Connection conn=null;
        PreparedStatement ps=null;
        ResultSet rs=null;
        try {
            conn=ConnectionFactory.getConnection();
            String query="SELECT idProdus, nume, stoc, pret  FROM Produs where nume=?";
            ps=conn.prepareStatement(query);
            ps.setString(1,nume);
            ps.execute();
            rs=ps.getResultSet();

            if (rs!=null && rs.next()){
                int id=rs.getInt("idProdus");
                String name=rs.getString("nume");
                int stoc=rs.getInt("stoc");
                float pret=rs.getFloat("pret");
                produs=new Produs(nume,stoc,pret);
                produs.setIdProdus(id);
                return produs;

            }

        }catch (Exception e){
            e.printStackTrace();
        }finally {
            ConnectionFactory.close(rs);
            ConnectionFactory.close(ps);
            ConnectionFactory.close(conn);
        }



        return produs;
    }

    public Produs findById(int id){
        Produs produs=null;
        Connection conn=null;
        PreparedStatement ps=null;
        ResultSet rs=null;
        try {
            conn=ConnectionFactory.getConnection();
            String query="SELECT idProdus, nume, stoc, pret  FROM Produs where idProdus=?";
            ps=conn.prepareStatement(query);
            ps.setInt(1,id);
            ps.execute();
            rs=ps.getResultSet();

            if (rs!=null && rs.next()){
                String name=rs.getString("nume");
                int stoc=rs.getInt("stoc");
                float pret=rs.getFloat("pret");
                produs=new Produs(name,stoc,pret);
                produs.setIdProdus(id);
                return produs;

            }

        }catch (Exception e){
            e.printStackTrace();
        }finally {
            ConnectionFactory.close(rs);
            ConnectionFactory.close(ps);
            ConnectionFactory.close(conn);
        }



        return produs;
    }

    public void delete(String nume){
        Connection conn=null;
        PreparedStatement ps=null;

        try{
            conn=ConnectionFactory.getConnection();
            String query="DELETE FROM Produs WHERE nume = ?";
            ps=conn.prepareStatement(query);
            ps.setString(1,nume);
            ps.execute();
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            ConnectionFactory.close(ps);
            ConnectionFactory.close(conn);
        }
    }

    public void updatePret(String nume,float val){
        Connection conn = null;
        PreparedStatement ps = null;
        try{
            conn=ConnectionFactory.getConnection();
            String query="UPDATE produs SET pret=? WHERE nume=?";
            ps=conn.prepareStatement(query);
            ps.setFloat(1,val);
            ps.setString(2,nume);
            ps.execute();

        }catch (Exception e){
            e.printStackTrace();
        }finally {
            ConnectionFactory.close(ps);
            ConnectionFactory.close(conn);
        }
    }

    public void updateStoc(String nume,int val){
        Connection conn = null;
        PreparedStatement ps = null;
        try{
            conn=ConnectionFactory.getConnection();
            String query="UPDATE produs SET stoc=? WHERE nume=?";
            ps=conn.prepareStatement(query);
            ps.setInt(1,val);
            ps.setString(2,nume);
            ps.execute();

        }catch (Exception e){
            e.printStackTrace();
        }finally {
            ConnectionFactory.close(ps);
            ConnectionFactory.close(conn);
        }
    }



}
