package dataAccessLayer;

import Model.Client;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

public class ClientDAO {

    public Client add(Client client){
        Connection conn=null;
        PreparedStatement ps=null;
        ResultSet rs=null;
        try{
            conn=ConnectionFactory.getConnection();
            String add = "INSERT INTO clienti(nume, prenume, parola, email, adress) VALUES(?, ?, ?, ?, ?)";
            ps=conn.prepareStatement(add, Statement.RETURN_GENERATED_KEYS);
            ps.setString(1,client.getNume());
            ps.setString(2,client.getPrenume());
            ps.setString(3,client.getParola());
            ps.setString(4,client.getEmail());
            ps.setString(5,client.getAdress());
            ps.execute();
            rs=ps.getGeneratedKeys();
            if (rs.next()){
                client.setIdClient(rs.getInt(1));
            }

        }catch (Exception e){
            e.printStackTrace();
        }finally {
            try {
                rs.close();
                conn.close();
                ps.close();
            }catch (Exception e){
                e.printStackTrace();
            }

        }
        return client;
    }

    public Client findByNume(String nume){

        Client client=null;
        Connection conn=null;
        PreparedStatement ps=null;
        ResultSet rs=null;
        try {
            conn=ConnectionFactory.getConnection();
            String query="SELECT idClient, nume, prenume, parola, email, adress FROM Clienti where nume=?";
            ps=conn.prepareStatement(query);
            ps.setString(1,nume);
            ps.execute();
            rs=ps.getResultSet();

            if (rs!=null && rs.next()){
                int id=rs.getInt("idClient");
                String name=rs.getString("nume");
                String prenume=rs.getString("prenume");
                String parola=rs.getString("parola");
                String email=rs.getString("email");
                String adress=rs.getString("adress");
                client=new Client(name,prenume,parola,email,adress);
                client.setIdClient(id);

                return client;
            }

        }catch (Exception e){
            e.printStackTrace();
        }finally {
            ConnectionFactory.close(rs);
            ConnectionFactory.close(ps);
            ConnectionFactory.close(conn);
        }



        return client;
    }
    public Client findById(int id){

        Client client=null;
        Connection conn=null;
        PreparedStatement ps=null;
        ResultSet rs=null;
        try {
            conn=ConnectionFactory.getConnection();
            String query="SELECT idClient, nume, prenume, parola, email, adress FROM Clienti where idClient=?";
            ps=conn.prepareStatement(query);
            ps.setInt(1,id);
            ps.execute();
            rs=ps.getResultSet();

            if (rs!=null && rs.next()){
                String name=rs.getString("nume");
                String prenume=rs.getString("prenume");
                String parola=rs.getString("parola");
                String email=rs.getString("email");
                String adress=rs.getString("adress");
                client=new Client(name,prenume,parola,email,adress);
                client.setIdClient(id);

                return client;
            }

        }catch (Exception e){
            e.printStackTrace();
        }finally {
            ConnectionFactory.close(rs);
            ConnectionFactory.close(ps);
            ConnectionFactory.close(conn);
        }



        return client;
    }

    public ArrayList<Client> showAllClints(){
        Connection conn=null;
        PreparedStatement ps=null;
        ResultSet rs=null;

        ArrayList<Client> clients=new ArrayList<Client>();
        try{
            conn=ConnectionFactory.getConnection();
            String query="SELECT * FROM Clienti";
            ps=conn.prepareStatement(query);
            ps.execute();
            rs=ps.getResultSet();

            while (rs.next()){
                int id=rs.getInt("idClient");
                String name=rs.getString("nume");
                String prenume=rs.getString("prenume");
                String parola=rs.getString("parola");
                String email=rs.getString("email");
                String adress=rs.getString("adress");

                Client client=new Client(name,prenume,parola,email,adress);
                client.setIdClient(id);
                clients.add(client);
            }
        }catch (Exception e){
            e.printStackTrace();
        }finally {

            ConnectionFactory.close(rs);
            ConnectionFactory.close(ps);
            ConnectionFactory.close(conn);
        }

        return clients;
    }

    public void delete(String nume){
        Connection conn=null;
        PreparedStatement ps=null;

        try{
            conn=ConnectionFactory.getConnection();
            String query="DELETE FROM Clienti WHERE nume = ?";
            ps=conn.prepareStatement(query);
            ps.setString(1,nume);
            ps.execute();
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            ConnectionFactory.close(ps);
            ConnectionFactory.close(conn);
        }
    }

    public void updateParola(String nume,String val){
        Connection conn = null;
        PreparedStatement ps = null;
        try{
            conn=ConnectionFactory.getConnection();
            String query="UPDATE Clienti SET parola=? WHERE nume=?";
            ps=conn.prepareStatement(query);
            ps.setString(1,val);
            ps.setString(2,nume);
            ps.execute();

        }catch (Exception e){
            e.printStackTrace();
        }finally {
            ConnectionFactory.close(ps);
            ConnectionFactory.close(conn);
        }
    }
    public void updateAdresa(String nume,String val){
        Connection conn = null;
        PreparedStatement ps = null;
        try{
            conn=ConnectionFactory.getConnection();
            String query="UPDATE Clienti SET adress=? WHERE nume=?";
            ps=conn.prepareStatement(query);
            ps.setString(1,val);
            ps.setString(2,nume);
            ps.execute();

        }catch (Exception e){
            e.printStackTrace();
        }finally {
            ConnectionFactory.close(ps);
            ConnectionFactory.close(conn);
        }
    }

    public void updateEmail(String nume,String val){
        Connection conn = null;
        PreparedStatement ps = null;
        try{
            conn=ConnectionFactory.getConnection();
            String query="UPDATE Clienti SET email=? WHERE nume=?";
            ps=conn.prepareStatement(query);
            ps.setString(1,val);
            ps.setString(2,nume);
            ps.execute();

        }catch (Exception e){
            e.printStackTrace();
        }finally {
            ConnectionFactory.close(ps);
            ConnectionFactory.close(conn);
        }
    }






}
