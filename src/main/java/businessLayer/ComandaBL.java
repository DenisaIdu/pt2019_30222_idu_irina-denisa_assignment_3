package businessLayer;

import Model.Client;
import Model.Comanda;
import Model.Produs;
import Model.Subcomanda;
import dataAccessLayer.ClientDAO;
import dataAccessLayer.ComandaDAO;

public class ComandaBL {

    public void addComada(Comanda comanda){
        ComandaDAO comandaDAO=new ComandaDAO();
        comandaDAO.addComanda(comanda);
    }
}
