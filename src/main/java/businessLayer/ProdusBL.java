package businessLayer;

import GUI.AddProductView;
import GUI.DeleteClientView;
import Model.Client;
import Model.Produs;
import dataAccessLayer.ClientDAO;
import dataAccessLayer.ProdusDAO;

public class ProdusBL {

    public void addProdus(AddProductView addProductView){
        String nume=addProductView.getTf1().getText();
        int stoc= Integer.parseInt(addProductView.getTf2().getText());
        float pret= Float.parseFloat(addProductView.getTf3().getText());

        Produs produs=new Produs(nume,stoc,pret);
        ProdusDAO produsDAO=new ProdusDAO();
        produsDAO.add(produs);
    }

    public void delete(String nume){
        ProdusDAO produsDAO=new ProdusDAO();
        produsDAO.delete(nume);
    }

    public void edit(String nume,String camp,String val){

        ProdusDAO produsDAO=new ProdusDAO();
        if (camp.equals("Pret")){
            float valoare= Float.parseFloat(val);
            produsDAO.updatePret(nume,valoare);
        }else
            if(camp.equals("Stoc")){
                int valoare= Integer.parseInt(val);
                produsDAO.updateStoc(nume,valoare);

        }
    }

    public Produs findByNume(String nume){
        ProdusDAO produsDAO=new ProdusDAO();
        Produs produs=produsDAO.findByName(nume);
        return produs;

    }



}
