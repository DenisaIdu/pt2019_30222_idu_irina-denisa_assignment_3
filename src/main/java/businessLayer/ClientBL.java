package businessLayer;

import GUI.ClientView;
import GUI.LogInView;
import Model.Client;
import dataAccessLayer.ClientDAO;

public class ClientBL {
    private String nume;
    public void delete(String nume){
        ClientDAO clientDAO=new ClientDAO();
        clientDAO.delete(nume);
    }

    public void logInClient(LogInView logInView){
        ClientDAO clientDAO=new ClientDAO();
        String nume=logInView.getTf1().getText();
        String pass=logInView.getP1().getText();

        Client c=clientDAO.findByNume(nume);
//        System.out.println(c.getNume());
        if (c.getParola().equals(pass)){
            this.nume=nume;
            ClientView clientView=new ClientView(c);
        }
        else{
            logInView.wrongPass();
        }

    }

}
