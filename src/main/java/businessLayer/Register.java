package businessLayer;

import GUI.RegisterView;
import Model.Client;
import dataAccessLayer.ClientDAO;
import dataAccessLayer.ConnectionFactory;

import java.sql.Connection;

public class Register {
    RegisterView registerView;

    public Register(RegisterView registerView){
        this.registerView=registerView;
        addClient();

    }

    public void addClient(){
        String nume=registerView.getTf2().getText();
        String pass=registerView.getP1().getText();
        String prenume=registerView.getTf5().getText();
        String email=registerView.getTf6().getText();
        String adress=registerView.getTf7().getText();

        Client c=new Client(nume,prenume,pass,email,adress);
        ClientDAO clientDAO=new ClientDAO();
        clientDAO.add(c);
    }




}
